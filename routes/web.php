<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('fbauth/{auth?}', 'Auth\LoginController@fbAuth');
Route::get('/', function () { return view('welcome');});
Route::get('profile', 'ProfileController@show')->name('profile');
Route::get('profile-edit', 'ProfileController@edit')->name('profile-edit');
Route::post('profile-edit', 'ProfileController@edit');

