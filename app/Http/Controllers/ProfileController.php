<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        $cfg = config('fb_auth.providers.Facebook.keys');
        $appID = $cfg['id'];
        $appSecret = $cfg['secret'];
        //dd($cfg);
        if($user->identifier !== null){
            $fb = new \Facebook\Facebook([
                'app_id' => $appID,
                'app_secret' => $appSecret,
                'default_graph_version' => 'v2.9',
            ]);
            try {
                $path='/'.$user->identifier.'?fields=id,name,posts{message,type,link,source,name,application,object_id}&access_token='
                    .$appID.'|'.$appSecret;
                $response = $fb->get($path);
                // Get the \Facebook\GraphNodes\GraphUser object for the current user.
                // If you provided a 'default_access_token', the '{access-token}' is optional.
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            $timeline = $response->getGraphUser()->asArray();
        }
        if(!isset($timeline)) $timeline=null;

        //var_dump($timeline);
        return view('profile', ['user' => $user, 'fb' => $timeline]);
    }

    public function edit()
    {
        if(request()->isMethod('post') && !empty(request()->all())){

            $rules = [
                'email' => 'required|email',
                'password' => 'same:password_confirm',
                'name' => 'required'
            ];

            $validation = Validator::make(request()->all(), $rules);
            if($validation->fails()){
                return redirect()->route('profile-edit')->withErrors($validation)->withInput();
            }

            $user = User::find(Auth::user()->id);
            $user->name = request()->get('name');
            $user->email = request()->get('email');
            if(request()->get('password')){
                $user->password = Hash::make(request()->get('password'));
            }

            if($user->save()){
                return redirect()->route('profile')->with('notify', 'Informacje zostały zaktualizowane.');
            }

            return redirect()->route('profile')->with('err-update', 'Nie udało się zaktualizować informacji.');
        }

        return view('profile_edit',['user' => Auth::user()]);
    }

}
