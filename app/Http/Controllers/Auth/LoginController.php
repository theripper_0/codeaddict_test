<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function fbAuth($auth = null)
    {
        if($auth == 'auth'){
            try{
                \Hybrid_Endpoint::process();
            }catch(\Exception $e){
                return redirect()->route('fbauth');
            }
            return;
        }
        try{
            $oauth = new \Hybrid_Auth(config_path().'/fb_auth.php');
            $provider = $oauth->authenticate('Facebook');
            $profile = $provider->getUserProfile();

        }catch(\Exception $e){
            return $e->getMessage();
        }
        //dd($profile);
        $user = User::where('identifier', $profile->identifier)->orWhere('email', $profile->email)->first();
        if(is_object($user) && ($user->identifier === null)){
            $user->identifier = $profile->identifier;
            $user->avatar = $profile->photoURL;
            $user->save();
        }
        if (is_object($user) && Auth::login($user)) return redirect()->route('profile');
        return redirect()->route('register')
            ->withInput([
                'name' => $profile->firstName.' '.$profile->lastName,
                'email' => $profile->email,
                'identifier' => $profile->identifier,
                'avatar' => $profile->photoURL
                ])
            ->with('notify','Nie znaleziono konta powiązanego z facebookiem. Wprowadź hasło żeby to zmienić');
    }

}
