@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="inline-block">{{ $user->name }}</h1>
                    <img src="{{ $user->avatar }}" class="img-circle pull-right">
                    <p>Twój adres e-mail: {{ $user->email }} </p>
                    <p>Data utworzenia konta: {{  $user->created_at }} </p>
                </div>
                @if(session('notify'))
                    <p class='text-center' style='color:green'>{{ session('notify') }}</p>
                @elseif(session('err-update'))
                    <p class='text-center' style='color:red'>{{ session('err-update') }}</p>
                @endif
                <div class="panel-body">
                    <h2 class="text-center">Timeline</h2>
                    @if(isset($fb['posts']))
                    @for ($k = 0; $k< count($fb['posts']); $k++)
                        @if((isset($fb['posts'][$k]['message']) || isset($fb['posts'][$k]['link']) ))
                    <div class="col-xs-12 item-vertical-padding @if( $k != (count($fb['posts'])-1)) bottom-bordered @endif">
                        {{-- $loop->last nie dziala z niewiadomego powodu, zastosowalem obejscie problemu --}}
                        @if(array_key_exists('message',$fb['posts'][$k]))
                            <h2>{{ $fb['posts'][$k]['message'] }}</h2>
                        @endif
                        @if(array_key_exists('link',$fb['posts'][$k]))
                            @if($fb['posts'][$k]['type'] =='video' && (array_key_exists('application',$fb['posts'][$k]) || preg_match('/youtube/',$fb['posts'][$k]['source'])))
                            <iframe class="yt"   width="100%" height="315" frameborder="0" src="{{ $fb['posts'][$k]['source'] }}" allowfullscreen>
                            </iframe>
                            @elseif($fb['posts'][$k]['type'] =='video')
                            <div class="fb-video" data-href="{{ $fb['posts'][$k]['link'] }}"  data-show-text="false">
                            </div>
                            @elseif($fb['posts'][$k]['type'] =='photo')
                                <div class="fb-post" data-href="{{ $fb['posts'][$k]['link'] }}" data-width="auto" data-show-text="true">
                                </div>
                            @elseif($fb['posts'][$k]['type'] =='link')
                                    <a href="{{ $fb['posts'][$k]['link'] }}">{{ $fb['posts'][$k]['name'] }}</a>

                            @endif
                        @endif
                    </div>
                        @endif
                    @endfor
                    @else
                    <p class="text-center">Na twojej tablicy facebooka nie ma jeszcze nic do wyświetlenia</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.9&appId=180794239111866";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

</script>
<script>
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }

    $('iframe[src*="youtu"].yt').each(function(){

       var url = $(this).attr('src');
        //console.log(url);
        var videoId = getId(url);
        var newUrl = '//www.youtube.com/embed/'+ videoId;
        $(this).attr('src', newUrl);
    });
</script>

@endsection
