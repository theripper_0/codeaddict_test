<?php
return [
    "base_url" => "http://codeaddict.lh/fbauth/auth",
    "providers" => [
        "Facebook" => [
            "enabled" => TRUE,
            "keys" => [
                "id" => "YOUR_APP_ID",
                "secret" => "YOUR_APP_SECRET"],
            "scope" => "email,user_posts,user_status",
        ]
    ]
];